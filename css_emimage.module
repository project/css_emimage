<?php

/**
 * Implementation of hook_help().
 */
function css_emimage_help($path, $arg) {
  switch ($path) {
    case 'admin/help#css_emimage':
      $output = '<p>'. t('Replaces image URLs in aggregated CSS files with embedded images when <em>CSS optimization</em> has been enabled in the <a href="@performance">Performance settings</a>.', array('@performance' => url('admin/settings/performance'))) .'</p>';
      return $output;
  }
}

/**
 * Implementation of hook_form_alter().
 */
function css_emimage_form_alter(&$form, &$form_state, $form_id) {
  if ($form_id == 'system_performance_settings') {
    $form['bandwidth_optimizations']['preprocess_css']['#description'] = (isset($form['bandwidth_optimizations']['preprocess_css']['#description'])
      ? $form['bandwidth_optimizations']['preprocess_css']['#description'] .' '
      : '')
      . t('Once the CSS files have been aggregated, image URLs will be replaced with embedded images.');
    $form['bandwidth_optimizations']['css_emimage_ielimit'] = array(
      '#type' => 'checkbox',
      '#title' => t('Only embed images less than 32KB'),
      '#description' => t('Internet Explorer does not support embedded images larger than 32KB. If you are not concerned about IE support you can ignore this limitation; otherwise, it is best to leave this checked.'),
      '#default_value' => variable_get('css_emimage_ielimit', 1),
    );
  }
}

/**
 * Implementation of hook_theme_registry_alter().
 *
 * Make css_emimage's page preprocess function run after everything else.
 * If the css_gzip module is installed, move it's preprocess function after ours.
 */
function css_emimage_theme_registry_alter(&$theme_registry) {
  if (isset($theme_registry['page'])) {
    // Move our preprocess function after everything else.
    if (($key = array_search('css_emimage_preprocess_page', $theme_registry['page']['preprocess functions'])) !== FALSE) {
      unset($theme_registry['page']['preprocess functions'][$key]);
    }
    $theme_registry['page']['preprocess functions'][] = 'css_emimage_preprocess_page';
    // Move css_gzip's preprocess function after ours.
    if (($key = array_search('css_gzip_preprocess_page', $theme_registry['page']['preprocess functions'])) !== FALSE) {
      unset($theme_registry['page']['preprocess functions'][$key]);
      $theme_registry['page']['preprocess functions'][] = 'css_gzip_preprocess_page';
    }
  }
}

/**
 * Implementation of hook_preprocess_hook().
 *
 * Replace URLs with data URIs in aggregated CSS files if CSS optimization is turned on.
 */
function css_emimage_preprocess_page(&$variables) {
  if (!empty($variables['styles']) && variable_get('preprocess_css', 0)) {
    $variables['styles'] = _css_emimage_process($variables['styles']);
  }
}

/**
 * Helper function to replace URLs with data URIs.
 */
function _css_emimage_process($styles) {
  $path_to_files_directory = base_path() . file_directory_path();
  $pattern = '/<link(.*?)href=".*?'. preg_quote($path_to_files_directory, '/') .'(.*?)(\?[^"]*)?"(.*?)\\/>/';
  if (preg_match_all($pattern, $styles, $matches) > 0) {
    foreach ($matches[2] as $i => $aggregated_file_name) {
      $file_path = file_directory_path();
      $aggregated_file_path = $file_path . $aggregated_file_name;
      $orig_file_name = str_replace('.css', '.orig.css', $aggregated_file_name);
      $orig_file_path = $file_path . $orig_file_name;
      $emimage_file_name = str_replace('.css', '.emimage.css', $aggregated_file_name);
      $emimage_file_path = $file_path . $emimage_file_name;

      // Save the processed CSS file if it doesn't exist yet.
      if (!file_exists($emimage_file_path) || filemtime($aggregated_file_path) > filemtime($emimage_file_path)) {
        _css_emimage_collect(NULL, TRUE);
        $contents = $orig_contents = file_get_contents($aggregated_file_path);
        $datauri_css = '';

        $pattern = '/([^{}]+){([^{}]*?(background(?:-image)?|list-style(?:-image)?):[^{};)]*?(?:none|url\([\'"]?.+?[\'"]?\))[^{}]*)}/i';
        $contents = preg_replace_callback($pattern, '_css_emimage_replace', $contents);

        if (!is_null($contents)) {
          foreach (_css_emimage_collect() as $data) {
            if ($data['datauri']) {
              $datauri_css .= implode(',', $data['selectors']) .'{'. $data['property'] .':'. $data['datauri'] . ($data['important'] ? ' !important' : '') .";}\n";
            }
            else if ($datauri_css) {
              // Only add these if the CSS has at least one data URI.
              $datauri_css .= implode(',', $data['selectors']) .'{'. $data['property'] .':'. $data['url'] . ($data['important'] ? ' !important' : '') .";}\n";
            }
          }

          // Save the modified aggregated CSS file.
          file_save_data($contents, $aggregated_file_path, FILE_EXISTS_REPLACE);
          // Save a copy of the original CSS for IE < 8 fallback.
          file_save_data($orig_contents, $orig_file_path, FILE_EXISTS_REPLACE);
        }
        else {
          $error_code = preg_last_error();
          $error_messages = array(PREG_NO_ERROR => 'NO_ERROR', PREG_INTERNAL_ERROR => 'INTERNAL_ERROR', PREG_BACKTRACK_LIMIT_ERROR => 'BACKTRACK_LIMIT_ERROR', PREG_RECURSION_LIMIT_ERROR => 'RECURSION_LIMIT_ERROR', PREG_BAD_UTF8_ERROR => 'BAD_UTF8_ERROR', PREG_BAD_UTF8_OFFSET_ERROR => 'BAD_UTF8_OFFSET_ERROR');
          watchdog('css_emimage', 'Error while trying to embed images in your CSS, falling back to unmodified CSS. PCRE error was: !error.',
            array('!error' => array_key_exists($error_code, $error_messages) ? $error_messages[$error_code] : $error_code), WATCHDOG_ERROR);
        }

        // Save the CSS file containing the embedded images.
        // This may be empty, but we use the file as a flag to prevent
        // processing the CSS on every uncached request.
        file_save_data($datauri_css, $emimage_file_path, FILE_EXISTS_REPLACE);
      }

      // Replace the aggregated file with the processed CSS file.
      if (file_exists($emimage_file_path) && filesize($emimage_file_path)) {
        $styles = str_replace($matches[0][$i],
          "<!--[if gte IE 8]><!-->\n". $matches[0][$i] ."\n". str_replace($aggregated_file_name, $emimage_file_name, $matches[0][$i]) ."\n<!--<![endif]-->\n"
          . "<!--[if lt IE 8]>\n". str_replace($aggregated_file_name, $orig_file_name, $matches[0][$i]) ."\n<![endif]-->",
          $styles);
      }
    }
  }

  return $styles;
}

/**
 * preg_replace_callback() callback to replace URLs with data URIs.
 */
function _css_emimage_replace($matches) {
  list($declaration, $selector, $properties) = $matches;
  $pattern = '/(background(?:-image)?|list-style(?:-image)?):[^{};)]*?((?:none|url\([\'"]?(.+?)[\'"]?\)))([^{};]*)/i';
  preg_match_all($pattern, $properties, $matches);
  foreach ($matches[1] as $i => $property) {
    $url = $matches[2][$i];
    $file = $matches[3][$i];
    $important = stripos($matches[4][$i], '!important') !== FALSE;
    if ($file && (strpos($file, base_path()) === 0) && ($image = image_get_info($file = substr($file, strlen(base_path()))))) {
      $ielimit = variable_get('css_emimage_ielimit', 1); // only embed data URIs less than 32KB, thanks IE
      if (!$ielimit || ($ielimit && ($image['file_size']*1.3333) < 32768)) {
        $declaration = str_replace($url, 'none', $declaration);
        _css_emimage_collect(array($selector, $property, $url, $file, $important, $image));
      }
      else {
        _css_emimage_collect(array($selector, $property, $url, $file, $important, NULL));
      }
    }
    else {
      _css_emimage_collect(array($selector, $property, $url, $file, $important, NULL));
    }
  }
  return $declaration;
}

/**
 * Helper function to collect CSS declarations to replace with data URIs.
 */
function _css_emimage_collect($info = NULL, $reset = FALSE) {
  static $data = array();
  if ($reset) {
    $data = array();
  }
  if (is_null($info)) {
    return $data;
  }

  if (is_array($info)) {
    list($selector, $property, $url, $file, $important, $image) = $info;

    $selector = trim($selector);

    // Normalize the CSS property name - allows us to collapse declarations in
    // some cases, and generated CSS is more consistent.
    if ($property == 'background' || $property == 'list-style') {
      $property .= '-image';
    }

    $current = array_pop($data);
    switch (TRUE) {
      case $current && ($current['property'] != $property || $current['file'] != $file || $current['important'] != $important):
        array_push($data, $current);
      case !$current:
        $current = array(
          'selectors' => array($selector),
          'property' => $property,
          'url' => $url,
          'file' => $file,
          'important' => $important,
          'datauri' => $image ? 'url(data:'. $image['mime_type'] .';base64,'. base64_encode(file_get_contents($file)). ')' : '',
        );
        break;
      default:
        $current['selectors'][] = $selector;
        break;
    }
    array_push($data, $current);
  }

  return $data;
}

